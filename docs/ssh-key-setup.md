# Generating and Managing SSH Keys for DDEV Deployment

This guide will walk you through the process of generating SSH keys and
understanding their roles in the DDEV deployment setup. SSH keys are essential
for establishing secure connections between your GitLab CI/CD pipeline and the
servers.

## Step 1: Generate SSH Keys

Generate SSH keys on your local machine to establish secure connections between
your GitLab CI/CD pipeline and the servers.

To generate SSH keys without overwriting existing keys, use the following
command:

```bash
ssh-keygen -t rsa -b 4096 -C "your_email@example.com" -f id_rsa_staging
```

This command will generate a pair of SSH keys (`id_rsa_staging`
and `id_rsa_staging.pub`) in the current directory. The private
key (`id_rsa_staging`) should be stored in your GitLab CI/CD variables
as `SSH_KEY_PRIVATE_STAGING`. The public key (`id_rsa_staging.pub`) will be used
during the server setup and stored in the `SSH_KEY_PUBLIC_STAGING` variable.

## Understanding SSH Key Roles

### GitLab CI/CD Variables

- **SSH_KEY_PRIVATE_STAGING**: The private SSH key for accessing the staging
  server. This key is stored in the GitLab CI/CD variables.
- **SSH_KEY_PUBLIC_STAGING**: The public SSH key corresponding to the staging
  server's private key. This key is added to the `authorized_keys` file on the
  staging server.

### SSH Key Usage

1. **GitLab Runner to Stage/Live Servers**:

   - The GitLab Runner uses `SSH_KEY_PRIVATE_STAGING` to authenticate and connect
     to the staging and live servers. These keys are stored in the GitLab CI/CD
     variables.

2. **Stage/Live Servers to SSHFS Server**:

   - During the installation of the stage and live servers, a new SSH key pair is
     generated. The public key from this key pair is displayed at the end of the
     installation. This public key should be added to the `authorized_keys` file
     on the SSHFS server. This allows the stage and live servers to securely
     mount the SSHFS server's directories.

## Setting Up SSH Keys on Servers

### Staging and Live Servers

After generating the SSH keys, follow these steps to set them up on the staging
and live servers:

1. **Store the Private Key in GitLab CI/CD Variables**:

   Add the private key to your GitLab CI/CD variables
   as `SSH_KEY_PRIVATE_STAGING`.

2. **Add the Public Key to the Server**:

   During the server setup, the public key `SSH_KEY_PUBLIC_STAGING` will be used
   to allow connections from the GitLab Runner.

### SSHFS Server

During the SSHFS server setup, you will be prompted to enter the public SSH key
generated from the stage and live servers. The setup script will automatically
add these keys to the `authorized_keys` file for the `storage` user.

## Summary of SSH Key Setup

- **Generate SSH Keys**: Create a pair of SSH keys for the staging server.
- **Store Private Keys**: Save the private keys in GitLab CI/CD variables.
- **Add Public Keys to Servers**: Add the public keys to the `authorized_keys`
  file on the respective servers.
- **Configure SSHFS Server**: Ensure the SSHFS server has the public keys of the
  staging and live servers added to its `authorized_keys` file.

By following these steps, you will establish secure SSH connections between your
GitLab CI/CD pipeline and the servers, enabling automated deployment and
management of your DDEV environments.

### Important Note

The public keys generated on the stage and live servers during installation need
to be added to the `authorized_keys` file on the SSHFS server. This allows stage
and live servers to connect to the SSHFS server securely.
