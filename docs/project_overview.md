# Project Overview

The DDEV Deployment Automation project offers a comprehensive solution for
managing development and production environments using DDEV. This project is
designed to automate the deployment and management of DDEV environments,
ensuring a smooth, efficient, and error-free workflow.

## Key Features

### Staging Environment

The staging environment allows you to create an isolated setup for testing and
validation before pushing changes to the live environment. Each merge request (
MR) triggers the creation of a unique staging environment with its own domain
name, making it accessible for review and testing. The domain names are
dynamically generated based on the merge request, ensuring that every change can
be individually reviewed and tested.

### Live Environment

The live environment hosts the production version of your application. It is
configured to use the primary domain specified in your DDEV setup. Live data can
be securely managed and accessed, ensuring that your production environment
remains stable and reliable.

### SSHFS Server

An optional SSHFS server can be utilized to mount remote file systems locally.
This is particularly useful for separating editorial folders from the source
code, allowing for better organization and management of content. For example,
media or file upload directories can be mounted read-only in the staging
environment to ensure data integrity.

### Database as a Service (DBaaS)

The project supports the use of DBaaS, enabling you to manage databases
efficiently. In the staging environment, databases are typically copied from the
live environment to ensure that the staging setup closely mirrors production.
This helps in identifying and resolving issues that may not be apparent in a
local development environment.

### Automatic SSL Configuration

SSL certificates are automatically configured for both staging and live
environments with ddev. This ensures secure communication and meets industry
standards for security.

### Dynamic Domain Names

Each merge request generates a unique domain name for the staging environment.
This domain is based on the merge request name, making it easy to access and
review specific changes. For example, a merge request named `feature-update` for
the project `example` would generate a domain
like `feature-update-example.staging.example.com`.

## Infrastructure Requirements

To utilize this project, you need to set up a few infrastructure components:

1. **Staging Server**: A server to host the staging environment.
2. **Live Server**: A server to host the live environment.
3. **Optional SSHFS Server**: A server to manage SSHFS mounts for separating
   content from source code.
4. **Database**: A DBaaS instance or a database server accessible from both
   staging and live environments.

## Configuration

The project requires a few configuration variables to be set up in your GitLab
CI/CD environment. These variables ensure that the deployment scripts can access
the necessary resources and perform the required operations. It is recommended
to set these variables at the group level in GitLab to ensure that they are
available for all projects within the group. This avoids the need to specify
these variables in each individual project.

With these components and configurations in place, the DDEV Deployment
Automation project can manage your environments effectively, providing a
seamless workflow from development to production.
