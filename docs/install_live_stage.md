# Installing Stage and Live Servers with DDEV

This guide will walk you through the process of setting up stage and live
servers for DDEV. The installation script
provided can be used for both servers. Make sure you are using Debian as the
operating system.

## Prerequisites

1. **GitLab Account and Group**: Create a GitLab account and set up a group.
   Within this group, define the necessary
   environment variables.
2. **Servers**: Purchase three servers accessible over the internet:

   - Live Server
   - Stage Server
   - Optional SSHFS Server: Used to separate editorial folders from the source
     code.

3. **DBaaS**: Ensure you have a Database as a Service (DBaaS) set up with
   accessible credentials.
4. **SSH Keys**: `SSH_KEY_PRIVATE_STAGING` and `SSH_KEY_PUBLIC_STAGING` need to
   be created before setting up the
   servers. Store both keys in your GitLab CI/CD variables.
5. **Partitions**: Ensure you have two partitions available on your servers. The
   UUIDs (Universally Unique Identifiers)
   of these partitions will be required during the setup. You can find the UUIDs
   of your partitions using the `blkid`
   command.

## Steps to Install Stage and Live Servers

### Step 1: Generate SSH Keys

Refer to
the [Generating and Managing SSH Keys for DDEV Deployment](ssh-key-setup.md)
guide to generate the necessary
SSH keys.

### Step 2: Download and Execute the Installation Script

Use `curl` or `wget` to download the installation script to your server, then
execute it. Below is an example of how to
do this for both stage and live servers.

#### Using `curl`

```bash
curl -O https://gitlab.com/mdys/ddev-deployment-automation/-/raw/main/scripts/install/install.server.ddev.live.sh
bash install.server.ddev.live.sh
```

#### Using `wget`

```bash
wget https://gitlab.com/mdys/ddev-deployment-automation/-/raw/main/scripts/install/install.server.ddev.live.sh
bash install.server.ddev.live.sh
```

### Step 3: Follow the Prompts

The script will prompt you for the following information:

- `SSH_KEY_PUBLIC_STAGING`: The public SSH key for staging.
- `MYSQL_SERVER_IP`: The IP address of your MySQL server.
- `MYSQL_SERVER_USER`: The MySQL user name.
- `MYSQL_SERVER_PASSWORD`: The MySQL password.
- `MYSQL_SERVER_PORT`: The MySQL port (default is 3306).
- Partition 1: The first partition for Docker storage.
- Partition 2: The second partition for DDEV storage.

#### Example Input

```bash
SSH_KEY_PUBLIC_STAGING: <your_ssh_public_key>
MYSQL_SERVER_IP: <your_mysql_server_ip>
MYSQL_SERVER_USER: <your_mysql_user>
MYSQL_SERVER_PASSWORD: <your_mysql_password>
MYSQL_SERVER_PORT: 3306
Partition 1: /dev/sda1
Partition 2: /dev/sdb1
```

### Step 4: Verify and Confirm

The script will output the entered values and ask for confirmation. Verify the
values and confirm to proceed.

### Step 5: Script Execution

The script will then:

- Format the specified partitions.
- Mount the partitions.
- Set up swap space.
- Install Docker and DDEV.
- Configure MySQL access.
- Set up SSH keys for the `ddev` user.
- Install and configure other necessary tools.

### Important Aspects

#### Partition Setup

The script formats and mounts the specified partitions for Docker and DDEV
storage. The UUIDs of the partitions are
added to `/etc/fstab` to ensure they are mounted on boot.

#### MySQL Configuration

The MySQL credentials are configured in `/home/ddev/.my.cnf` for easy access.

#### SSH Key Setup

The public SSH key is added to `/home/ddev/.ssh/authorized_keys`.

#### Certificate Installation and Key Generation

```bash
runuser -l ddev -c 'mkcert -install'
runuser -l ddev -c 'ssh-keygen'
```

These commands are run as the `ddev` user. The first command installs `mkcert`,
a tool for creating locally-trusted
development certificates. The second command generates an SSH key pair for
the `ddev` user.

#### Setting Up a Cron Job

```bash
runuser -l ddev -c 'echo "0 3 * * * ddev restart --all" | crontab -'
```

This command sets up a cron job for the `ddev` user to restart all DDEV projects
every day at 3 AM. This helps ensure
that the environments are kept fresh and running smoothly.

### Retrieving the Public Key for the SSHFS Server

```bash
cat /home/ddev/.ssh/id_rsa.pub
```

This command outputs the public key generated for the `ddev` user. You need to
copy this public key to your SSHFS
server's `authorized_keys` file to allow the stage and live servers to connect.

### Understanding SSH Keys and GitLab Runner Usage

- **SSH_KEY_PRIVATE_STAGING**: Stored in GitLab CI/CD variables, used by the
  GitLab Runner to authenticate and connect
  to the stage server.
- **SSH_KEY_PUBLIC_STAGING**: Added to the stage server to allow connections
  from the GitLab Runner.
- **Public Key Generated on the Server**: This key, generated during the
  installation process, is used to connect to the
  SSHFS server.

These keys ensure secure communication between your GitLab CI/CD pipeline and
the servers. After setting up the server,
the public key generated on the server will be used to grant access to the SSHFS
server, and the private key stored in
GitLab will be used by the pipeline for SSH authentication.

### Example Configuration for GitLab CI/CD

In your GitLab CI/CD settings, ensure the necessary variables are defined as
mentioned earlier. This will automate the
deployment process across your environments.

### Conclusion

By following these steps, you can set up and utilize DDEV deployment automation
scripts on your stage and live servers,
ensuring efficient and reliable deployments. The provided installation script
simplifies the setup process, and the
environment variables defined in GitLab CI/CD settings ensure consistency across
your projects.
