#!/bin/bash

echo "Running scripts/staging/typo3.sh"

# Check necessary variables
check_variable "PROJECT_FOLDER"
check_variable "DATABASE_NAME"
check_variable "MYSQL_SERVER_IP"
check_variable "MYSQL_SERVER_PASSWORD"
check_variable "MYSQL_SERVER_PORT"
check_variable "MYSQL_SERVER_USER"
check_variable "DDEV_TYPE"
check_variable "DEPLOY_TYPE"

if [ "$DDEV_TYPE" == "typo3" ]; then
  # Remove comments from AdditionalConfiguration file to not allow ddev to overwrite it each time ddev restarts
  sed -i '/^\s\*/d' "$PROJECT_FOLDER/public/typo3conf/AdditionalConfiguration.php" || error_exit "Failed to remove comments from AdditionalConfiguration.php"
  sed -i '/^\/\*/d' "$PROJECT_FOLDER/public/typo3conf/AdditionalConfiguration.php" || error_exit "Failed to remove comments from AdditionalConfiguration.php"

  if [ "$DEPLOY_TYPE" == "live" ]; then
    # Set mysql connection to MYSQL DBSassS only when deploying to live
    sed -i "s/'dbname' => 'db'/'dbname' => '$DATABASE_NAME'/g" "$PROJECT_FOLDER/public/typo3conf/AdditionalConfiguration.php" || error_exit "Failed to update dbname in AdditionalConfiguration.php"
    sed -i "s/'host' => 'db'/'host' => '$MYSQL_SERVER_IP'/g" "$PROJECT_FOLDER/public/typo3conf/AdditionalConfiguration.php" || error_exit "Failed to update host in AdditionalConfiguration.php"
    sed -i "s/'password' => 'db'/'password' => '$MYSQL_SERVER_PASSWORD'/g" "$PROJECT_FOLDER/public/typo3conf/AdditionalConfiguration.php" || error_exit "Failed to update password in AdditionalConfiguration.php"
    sed -i "s/'port' => '3306'/'port' => '$MYSQL_SERVER_PORT'/g" "$PROJECT_FOLDER/public/typo3conf/AdditionalConfiguration.php" || error_exit "Failed to update port in AdditionalConfiguration.php"
    sed -i "s/'user' => 'db'/'user' => '$MYSQL_SERVER_USER'/g" "$PROJECT_FOLDER/public/typo3conf/AdditionalConfiguration.php" || error_exit "Failed to update user in AdditionalConfiguration.php"
  fi
fi

# @todo: move this to typo3.sh
if [ "$DDEV_TYPE" == "typo3" ]; then
  if [ -d "$PROJECT_FOLDER/config" ]; then
    echo "Change ddev domains for staging: $DOMAIN.ddev.site > $DDEV_DOMAIN"
    find "$PROJECT_FOLDER/config" -name "config.yaml" -exec sed -i "s/$DOMAIN.ddev.site/$DDEV_DOMAIN/g" {} +
  fi

  if [ -f "$PROJECT_FOLDER/codecept.conf.js" ]; then
    echo "Change ddev domains inside codecept.conf.js: $DOMAIN.ddev.site > $DDEV_DOMAIN"
    sed -i "s/$DOMAIN.ddev.site/$DDEV_DOMAIN/g" "$PROJECT_FOLDER/codecept.conf.js"
  fi
fi
