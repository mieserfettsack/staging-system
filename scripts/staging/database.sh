#!/bin/bash

echo "Running scripts/staging/database.sh"

# Check if required environment variables are set
check_variable "$DEPLOY_TYPE" "DEPLOY_TYPE"
check_variable "$DATABASE_NAME" "DATABASE_NAME"
check_variable "$DEPLOY_TYPE" "DEPLOY_TYPE"

# Define the SQL query to check for read permission on the database
QUERY_CHECK_READ_PERMISSION_DATABASE="SELECT 1 FROM information_schema.SCHEMATA WHERE schema_name = '$DATABASE_NAME' LIMIT 1;"

# Execute the query and capture the result (including any errors)
RESULT=$(mysql "$DATABASE_NAME" -e "$QUERY_CHECK_READ_PERMISSION_DATABASE" 2>&1)

# Check if the query execution was successful
if ! mysql "$DATABASE_NAME" -e "$QUERY_CHECK_READ_PERMISSION_DATABASE" > /dev/null 2>&1; then
  echo "ERROR: Cannot read from database '$DATABASE_NAME', or the database does not exist."
  echo "MySQL error: $RESULT" # Provide detailed MySQL error for debugging
  RUN_DATABASE_OPERATIONS=false
else
  echo "SUCCESS: Database '$DATABASE_NAME' is accessible and exists."
  RUN_DATABASE_OPERATIONS=true
fi

# Check if the NO_DATABASE flag is set (indicating no database should be used)
if [ -n "$NO_DATABASE" ]; then
  echo "INFO: no-database inside project.yaml is set to true. Skipping database operations."
  RUN_DATABASE_OPERATIONS=false
fi

# Check if NO_DB_IMPORT_FROM_LIVE flag is set (indicating no import from live database)
if [ "$NO_DB_IMPORT_FROM_LIVE" == "true" ]; then
  echo "INFO: no-db-import-from-live inside project.yaml is set to true. Skipping database import."
  RUN_DATABASE_OPERATIONS=false
fi

# Proceed with database operations if allowed
if [ "$RUN_DATABASE_OPERATIONS" = true ]; then
  # Check if this is a live deployment
  if [ "$DEPLOY_TYPE" == "live" ]; then
    echo "INFO: Live deployment. No database to import. Using $DATABASE_NAME."

    # Add database connections for Laravel projects
    if [ "$DDEV_TYPE" == "laravel" ]; then
      ENV_FILE="$PROJECT_FOLDER/.env.live"

      # Append new values to the .env file
      # shellcheck disable=SC2129
      echo 'DB_CONNECTION="mariadb"' >> "$ENV_FILE"
      echo 'DB_HOST="'"$MYSQL_SERVER_IP"'"' >> "$ENV_FILE"
      echo 'DB_PORT="'"$MYSQL_SERVER_PORT"'"' >> "$ENV_FILE"
      echo 'DB_DATABASE="'"$DATABASE_NAME"'"' >> "$ENV_FILE"
      echo 'DB_USERNAME="'"$MYSQL_SERVER_USER"'"' >> "$ENV_FILE"
      echo 'DB_PASSWORD="'"$MYSQL_SERVER_PASSWORD"'"' >> "$ENV_FILE"

      # Confirmation message
      echo "Database values in .env file successfully updated."
    fi
  else
    # Check if the database exists before attempting to import
    if database_exists "$DATABASE_NAME"; then
      echo "INFO: Importing database from $DATABASE_NAME to ddev's database container."
      mysqldump --no-tablespaces "$DATABASE_NAME" > database.sql || error_exit "ERROR: Failed to dump database"
      ddev import-db --no-progress --file=./database.sql || error_exit "ERROR: Failed to import database"
      rm database.sql
    else
      echo "INFO: Database $DATABASE_NAME does not exist. Skipping import."
    fi
  fi
fi
