#!/bin/bash

echo "Running scripts/staging/sshfs.sh"

# Check necessary variables
check_variable "$PROJECT_CONFIG_FILE" "PROJECT_CONFIG_FILE"
check_variable "$STAGING_STORAGE_USER" "STAGING_STORAGE_USER"
check_variable "$STAGING_STORAGE_IP" "STAGING_STORAGE_IP"
check_variable "$PROJECT_FOLDER" "PROJECT_FOLDER"

if [ -f "$PROJECT_CONFIG_FILE" ]; then
  # Get comma-separated list of folders to mount read-only from live server
  SSHFS_MOUNTS=$( \
    shyaml -q get-values sshfs-mounts < "$PROJECT_CONFIG_FILE" | \
    sed -z "s/\n/,/g;s/,$/\n/" ) || error_exit "Failed to get SSHFS_MOUNTS from PROJECT_CONFIG_FILE"

  # Proceed only if SSHFS_MOUNTS is not empty
  if [ -n "$SSHFS_MOUNTS" ]; then
    # Create directory for project if it doesn't exist
    ssh -n "$STAGING_STORAGE_USER@$STAGING_STORAGE_IP" "mkdir -p /mnt/storage/$SSH_PROJECT_NAME_SLUG" || error_exit "Failed to create directory on storage server"
    for FOLDER_NAME in ${SSHFS_MOUNTS//,/ }; do
      FOLDER_SLUG=$(create_slug "$FOLDER_NAME") || error_exit "Failed to create slug for folder name"
      ssh -n "$STAGING_STORAGE_USER@$STAGING_STORAGE_IP" "mkdir -p /mnt/storage/$SSH_PROJECT_NAME_SLUG/$FOLDER_NAME" || error_exit "Failed to create folder on storage server"
      mkdir -p "$PROJECT_FOLDER/$FOLDER_NAME" || error_exit "Failed to create local folder"
      sshfs "$STAGING_STORAGE_USER@$STAGING_STORAGE_IP:/mnt/storage/$SSH_PROJECT_NAME_SLUG/$FOLDER_NAME" "$PROJECT_FOLDER/$FOLDER_NAME" -o "$MOUNT_OPTIONS" || error_exit "Failed to mount folder using sshfs"

      # Add a separate ddev composer file for each sshfs mount
      echo "Create additional docker-compose file: $PROJECT_FOLDER/.ddev/docker-compose.mount.$FOLDER_SLUG.yaml with this mount:"
      echo "$PROJECT_FOLDER/$FOLDER_NAME:/var/www/html/$FOLDER_NAME"
      cat << EOT > "$PROJECT_FOLDER/.ddev/docker-compose.mount.$FOLDER_SLUG.yaml"
services:
  web:
    volumes:
      - "$PROJECT_FOLDER/$FOLDER_NAME:/var/www/html/$FOLDER_NAME"
EOT
    done
  else
    echo "No SSHFS mounts defined in $PROJECT_CONFIG_FILE. Skipping SSHFS setup."
  fi
else
  error_exit "Project configuration file not found: $PROJECT_CONFIG_FILE"
fi
