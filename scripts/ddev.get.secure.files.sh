#!/bin/bash

# Function to print an error message and exit
error_exit() {
  echo "$1" 1>&2
  exit 1
}

# Function to fetch secure file ID by name
fetch_secure_file_id() {
  local file_name="$1"
  local secure_files_list="$2"
  echo "$secure_files_list" | jq -r --arg FILE_NAME "$file_name" '.[] | select(.name == $FILE_NAME) | .id'
}

# Check if .ddev/project.yaml exists and read secure_files if it does
if [ -f ./.ddev/project.yaml ]; then
  echo "Running scripts/ddev.get.secure.files.sh"

  # Read secure_files from .ddev/project.yaml
  SECURE_FILES=$(shyaml -q get-value secure-files < ./.ddev/project.yaml)

  if [ -z "$SECURE_FILES" ]; then
    echo "No secure-files defined in .ddev/project.yaml. Skipping download."
  else
    IFS=',' read -r -a FILE_ARRAY <<< "$SECURE_FILES"

    # Fetch the list of secure files from GitLab API
    SECURE_FILES_LIST=$(curl --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" --silent "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/secure_files")
    if ! curl --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" --silent "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/secure_files" -o /dev/null; then
      error_exit "Failed to fetch secure files list from GitLab API"
    fi

    # Download each file from GitLab Secure Files
    for FILE in "${FILE_ARRAY[@]}"; do
      FILE_ID=$(fetch_secure_file_id "$FILE" "$SECURE_FILES_LIST")

      if [ -z "$FILE_ID" ]; then
        echo "File $FILE not found in secure files list. Skipping."
        continue
      fi

      echo "Downloading FILE: $FILE with ID: $FILE_ID"
      RESPONSE=$(curl --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" --output "$FILE" --write-out "%{http_code}" --silent --show-error "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/secure_files/$FILE_ID/download")

      if [ "$RESPONSE" -ne 200 ]; then
        error_exit "Failed to download $FILE. HTTP status code: $RESPONSE"
      fi
    done
  fi
else
  echo "No .ddev/project.yaml found, skipping scripts/ddev.get.secure.files.sh"
fi
